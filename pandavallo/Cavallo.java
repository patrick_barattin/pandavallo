package pandavallo;

public class Cavallo {
	
	private int colore;
	private int posizione;

	public Cavallo(int colore) {
		this.colore=colore;
		posizione=0;
	}
	
	public int getColore() {
		return colore;
	}
	
	public int getPosizione() {
		return posizione;
	}
	
	boolean haVinto() {
		return posizione>7;
	}

	public void passoAvanti() {
		posizione++;
	}
	
	public void passoIndietro() {
		posizione--;
	}

}
