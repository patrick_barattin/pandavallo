package pandavallo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by patrick on 02/02/16.
 */
public class NewUserDialog extends JDialog implements ActionListener {

    JPanel panel = new JPanel();
    JLabel lblUsername = new JLabel("Username:");
    JLabel lblPassword = new JLabel("Password:");
    JLabel lblPasswordConfirm = new JLabel("Ripeti la password");
    JTextField txtUsername = new JTextField(20);
    JPasswordField txtPassword = new JPasswordField(20);
    JPasswordField txtPasswordConfirm = new JPasswordField(20);
    JButton btnNew = new JButton("Create User");

    ClientLogica clientLogica;

    public NewUserDialog(ClientLogica clientLogica) {

        setModal(true);
        this.clientLogica = clientLogica;


        btnNew.addActionListener(this);


        panel.add(lblUsername);
        panel.add(txtUsername);
        panel.add(lblPassword);
        panel.add(txtPassword);
        panel.add(lblPasswordConfirm);
        panel.add(txtPasswordConfirm);
        panel.add(btnNew);
        getContentPane().add(BorderLayout.CENTER, panel);


        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);


        setSize(250,300);

        setVisible(true);

        setTitle("Pandavallo");
        // pack();


    }



    public void actionPerformed(ActionEvent e) {
        String utente = txtUsername.getText();
        String password = String.valueOf(txtPassword.getPassword());
        String passwordConfirm = String.valueOf(txtPasswordConfirm.getPassword());
        utente.replaceAll("\\s","");
        password.replaceAll("\\s","");
        passwordConfirm.replaceAll("\\s","");


        if (utente.length() == 0 || clientLogica.contrallaStringhe(utente)) {
            JOptionPane.showMessageDialog(null, "Utente non può essere vuoto o contenere \",\"", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
        } else clientLogica.setUtente(utente);

        if (password.length() == 0|| clientLogica.contrallaStringhe(password)){
            JOptionPane.showMessageDialog(null,"La password non può essere vuota o contenere \",\"","ATTENZIONE",JOptionPane.ERROR_MESSAGE);
        } else if (!password.equals(passwordConfirm)) {
            JOptionPane.showMessageDialog(null,"Le password non corrispondono","ATTENZIONE",JOptionPane.ERROR_MESSAGE);



        } else {

            clientLogica.setPassword(password);
            clientLogica.setAzione(2);
        }


        dispose();


    }

    public int getAzione() {
        return clientLogica.getAzione();
    }
}
