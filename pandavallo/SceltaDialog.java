package pandavallo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by patrick on 02/02/16.
 */
public class SceltaDialog extends JDialog implements ActionListener {
    int scelta;
    JPanel panel =new JPanel();
    JButton login = new JButton("Login");
    JButton newUser  = new JButton("Sign in");

    public SceltaDialog(){
        setModal(true);




        login.addActionListener(this);
        newUser.addActionListener(this);

        panel.add(login);
        panel.add(newUser);
        getContentPane().add(BorderLayout.CENTER,panel);


        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);


        setSize(250,100);

        setVisible(true);
        pack();

    }

    public void  actionPerformed(ActionEvent e){

        if (e.getSource() == login){
            scelta =1;

        } else scelta =2;

        dispose();



    }

    public int getScelta(){
        return scelta;
    }
}
