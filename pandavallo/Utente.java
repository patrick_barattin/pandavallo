package pandavallo;

public class Utente {


	String nome;
	int cavallo;
	int puntata;
	int credito;
	boolean vinto;

	public Utente(String nome, int cavallo, int puntata) {
		this.nome = nome;
		this.cavallo = cavallo;
		this.puntata = puntata;
		this.vinto=false;

	}
	public Utente(){}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getNome() {
		return nome;
	}


	public int getCavallo() {
		return cavallo;
	}

	public void setCavallo(int cavallo) {
		this.cavallo = cavallo;
	}


	public int getPuntata() {
		return puntata;
	}

	public void setPuntata(int puntata) {
		this.puntata = puntata;
	}


	public boolean isVinto() {
		return vinto;
	}

	public void setVinto(boolean vinto) {
		this.vinto = vinto;
	}

	public int getCredito() {
		return credito;
	}

	public void setCredito(int credito) {
		this.credito = credito;
	}
	
	
	/*
	 * IL CLIENT MANDA AL SERVER NOME, CAVALLO SU CUI PUNTARE E PUNTATA
	 */
}
