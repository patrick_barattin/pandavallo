package pandavallo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
	
	ServerSocket socket;
    Socket[] clientsLogged=new Socket[10];
    int numeroClientLogged =0;
    long current =0;
    long passed;
    ClientSocket clientSocket =  new ClientSocket();
    ArrayList<Utente>utenteArrayList = new ArrayList<Utente>();



    public Server()  {
		try {
			socket=new ServerSocket(4848);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		while(true){
			 
			try {
				
				Socket client ;

				if (getNumeroClientLogged()>=2){
                    if (isTimeOut()) startGame();
                }


				client = socket.accept();




				Thread newThread=new Thread(new Runnable() {
                    final Socket finalClient = client;



                    @Override
					public void run() {
                        ServerLogin login  =new ServerLogin(finalClient, utenteArrayList);
                        try {
                            login.join();
                            System.out.println("uscito");
                            if(login.isLogged()){
                                addClient(login.getClientSocket().getObjectInputStream(),login.getClientSocket().getObjectOutputStream());

                                utenteArrayList.add(login.getUtente());
                                if (current ==0) startTime();

                                if(getNumeroClientLogged() >= 10){
                                    startGame();
                                }else if (getNumeroClientLogged()>=2 && isTimeOut()){
                                    startGame();
                                }

                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }


                    }
				});
				
				newThread.start();
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}

			
		}
		
	}

    public void addClient(ObjectInputStream objectInputStream, ObjectOutputStream objectOutputStream){

        clientSocket.setObjectInputStreamsLogged(objectInputStream,numeroClientLogged);
        clientSocket.setObjectOutputStreamsLogged(objectOutputStream,numeroClientLogged);
        numeroClientLogged ++;

    }

    public int getNumeroClientLogged(){
        return numeroClientLogged;
    }

    public void startTime(){
        current=System.currentTimeMillis();


    }



    public boolean isTimeOut(){
        passed+=System.currentTimeMillis()-current;
        if(passed >= 60000)return true;
        else return false;
    }

    public void startGame(){
        System.out.println("start game");
        Thread newThread=new Thread(new Runnable() {
            @Override
            public void run() {

        ServerLogica logica=new ServerLogica(clientSocket, numeroClientLogged);
                reset();

                Utente[] utenti;
                try {
                    logica.join();
                    utenti=logica.getUser();
                    for (int i =0; i<utenti.length;i++){
                        String username = utenti[i].getNome();

                        for (int j=0; j<utenteArrayList.size(); j++){
                           String username2= utenteArrayList.get(j).getNome();
                            if(username.equals(username2)) utenteArrayList.remove(j);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        newThread.start();



    }

    public void reset(){
        numeroClientLogged = 0;
        clientSocket = new ClientSocket();
        current=0;
        passed = 0;

    }

	public static void main(String[] args) {
            new Server();

    }
}
