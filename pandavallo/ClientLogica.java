package pandavallo;

/**
 * Created by patrick on 26/01/16.
 */
public class ClientLogica {
    public static final int ROSSO = 1;
    public static final int NERO = 2;
    public static final int BLU = 3;
    public static final int VERDE = 4;

    public int posizioneCavalloRosso = 9;
    public int posizioneCavalloNero = 18;
    public int posizioneCavalloBlu = 27;
    public int posizioneCavalloVerde = 36;

    String utente;
    int cabballo;
    int puntata =0;
    String password;
    int credito;



    int azione =0; //se 0 ha chiuso il login, se 1 si logga, se 2 nuovo utente

    public void reset(){
        posizioneCavalloRosso = 9;
        posizioneCavalloNero = 18;
        posizioneCavalloBlu = 27;
        posizioneCavalloVerde = 36;
        puntata =0;
    }

    public int getGridPosition(int cavallo) {

       /* if (cavallo != NERO || cavallo != ROSSO || cavallo != VERDE || cavallo != BLU) {
            throw new RuntimeException("Inserire il il numero di un cavallo");
        }   perch? mi dice che è una condizione sempre vera? */
        switch (cavallo) {
            case ROSSO:
                return posizioneCavalloRosso;
            case NERO:
                return posizioneCavalloNero;
            case BLU:
                return posizioneCavalloBlu;
            case VERDE:
                return posizioneCavalloVerde;

            default:
                return 0;
        }

    }



    public void updateGridPosition(int cavallo, int mossa) {
        /*if (cavallo != NERO || cavallo != ROSSO || cavallo != VERDE || cavallo != BLU) {
            throw new RuntimeException("Inserire il il numero di un cavallo");
        }*/
        if (mossa == 1) {


            switch (cavallo) {
                case ROSSO:
                    posizioneCavalloRosso +=1;
                    break;
                case NERO:
                    posizioneCavalloNero +=1;
                    break;
                case BLU:
                    posizioneCavalloBlu +=1;
                    break;
                case VERDE:
                    posizioneCavalloVerde +=1;
                    break;


            }

        }
        else if (mossa == 0) {
            switch (cavallo) {
                case ROSSO:
                    posizioneCavalloRosso -=1;
                    break;
                case NERO:
                    posizioneCavalloNero -=1;
                    break;
                case BLU:
                    posizioneCavalloBlu -=1;
                    break;
                case VERDE:
                    posizioneCavalloVerde -=1;
                    break;


            }

        }


    }
    public void setPuntata(int puntata) {
        this.puntata = puntata;
    }

    public void setCabballo(int cabballo) {
        this.cabballo = cabballo;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }
    public int getPuntata() {
        return puntata;
    }

    public String getUtente() {
        return utente;
    }

    public int getCabballo() {
        return cabballo;
    }

    public void setPassword(String password){ this.password = password;}

    public String getPassword(){return password;}
    public int getAzione() {
        return azione;
    }

    public void setAzione(int azione) {
        this.azione = azione;
    }

    public String getCavalloName( int i) {


        switch (i){
            case ROSSO: return "Rosso";

            case NERO: return "Nero";

            case VERDE: return "Verde";

            case BLU:return "Blu";

            default: return "";



        }



    }

    public boolean contrallaStringhe(String s){
        return s.contains(",");
    }



    public int getCredito() {
        return credito;
    }

    public void setCredito(int credito) {
        this.credito = credito;
    }



}
