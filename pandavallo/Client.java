package pandavallo;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/** Created by patrick on 27/01/16. */
public class Client {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket client;
	private String host;
	ClientGUI clientGUI;

	public Client(String host) {
		this.host = host;
		clientGUI = new ClientGUI();
	}

	void runClient(int status) {
		try {
			connectToServer(status);
			getSteams();
			procesConnection(status);
		} catch (EOFException eof) {
			System.err.print("closeConnection");
		} catch (IOException e) {
		} catch (ClassNotFoundException cl) {
		} finally {
			closeConnection();
		}
	}

	void connectToServer(int status) throws IOException {
        //se status =0 prima connessione
        //se status =1 errore di connessione
        //se status =3 nuova partita
        if (status !=3) {
            if (clientGUI.loginGUI() == 0) {

                JOptionPane.showMessageDialog(null, "Accesso non effettuato", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
                System.exit(1);


            }
            if (status == 0) clientGUI.logGUI();

            clientGUI.updateLogArea("Utente= " + clientGUI.getUtente());
        }


		//l'ho messo qua così prima di connettersi mette dentro username and pw
		client = new Socket(InetAddress.getByName(host), 4848);
	}

	void getSteams() throws IOException {
		input = new ObjectInputStream(client.getInputStream());

		output = new ObjectOutputStream(client.getOutputStream());
		//output.flush();

	}

	void procesConnection(int status) throws IOException, ClassNotFoundException {
    String message;
        if (status ==3){
            message= clientGUI.getAzione() + "," + clientGUI.getUtente() +"," + clientGUI.getPassword();
            output.writeObject(message);



        }else {
            switch (clientGUI.getAzione()) {
                case 1:
                    //1,nomeutente,password
                    message = clientGUI.getAzione() + "," + clientGUI.getUtente() + "," + clientGUI.getPassword();
                    output.writeObject(message);

                    break;
                case 2:
                    //2,nomeutente,password
                    message = clientGUI.getAzione() + "," + clientGUI.getUtente() + "," + clientGUI.getPassword();
                    output.writeObject(message);
                    break;
                case 0:
                    JOptionPane.showMessageDialog(null, "Accesso non effettuato", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
                    System.exit(1);


            }
        }
        System.out.println("inviato");
        message = (String) input.readObject();

		String serverinfo[]= message.split(",");

        System.out.println(message);

        if (serverinfo[0].equals("LOGGED")) {
			clientGUI.setCredito(Integer.parseInt(serverinfo[1]));

            clientGUI.updateLogArea("Login effettuato \n In attesa di altri giocatori");
			clientGUI.updateLogArea("Il tuo credito è di: " + clientGUI.getCredito() + " pandaCoin");
        }
        else if(message.equals("NOTLOGGED")){
            clientGUI.updateLogArea("Accesso negato, Utente o Password errati ");
            runClient(1);
        }
		else if(message.equals("USER ALREADY EXIT")){
            clientGUI.updateLogArea("Utente già esistente ");
            runClient(1);
        }
        else if (message.equals("USER ALREADY LOGGED")){
            clientGUI.updateLogArea("Utente già in gioco");
            runClient(1);
        }




        message = (String) input.readObject();
        System.out.println(message);


        if (message.equals("START")) {

			if (status != 3)clientGUI.gameGUI();

           while (clientGUI.getPuntata() <=0 || clientGUI.getPuntata() > clientGUI.getCredito()) {

                clientGUI.setPuntata(clientGUI.puntaGUI());

                if (clientGUI.getPuntata() <= 0)
                    JOptionPane.showMessageDialog(null, "La puntata deve essere maggiore di 0", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
                if(clientGUI.getPuntata() > clientGUI.getCredito())
                	JOptionPane.showMessageDialog(null, "Credito insufficiente", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
            }


			String puntata = clientGUI.getUtente()+","+clientGUI.getPuntata()+","+clientGUI.getCabballo();

			output.writeObject(puntata);



            clientGUI.updateLogArea("Hai puntato il cavalllo "+ clientGUI.getCavalloName(clientGUI.getCabballo()) +" puntado: " + clientGUI.getPuntata());





			int a = (Integer) input.readObject();
            System.out.println("message: " + a);

            for(int i=0;i<a;i++){

                String utenti = (String) input.readObject();

                String infoClient[] =new String[3];
                infoClient = utenti.split(",");

                String update  = infoClient[0] + " ha selezionato il cavallo " + clientGUI.getCavalloName(Integer.parseInt(infoClient[1]))+" puntando "+ infoClient[2];

                clientGUI.updateLogArea(update);
            }




			int mossa=0;
			int cavallo=0;
			int colonna =0;
            boolean vinto=false;
			do {

				String mossaString= (String) input.readObject();
                mossa=Integer.parseInt(mossaString.split(",")[1]);
				cavallo=Integer.parseInt(mossaString.split(",")[0]);
				colonna=Integer.parseInt(mossaString.split(",")[2]);

				if(mossa!=2)
					clientGUI.updateConnectionInfo("Cavallo "+clientGUI.getCavalloName(cavallo)+
							(mossa==1 ? " avanti" : " indietro"));
				else{
                    clientGUI.updateConnectionInfo("Cavallo "+clientGUI.getCavalloName(cavallo)+
                            " vince!");
                    if (clientGUI.getCabballo()==cavallo) vinto = true;
                }

				clientGUI.updateGridPosition(cavallo, mossa);

				clientGUI.updateCarte(mossa,cavallo,colonna);
				clientGUI.updateCavalli();
			} while(mossa!=2);

            if (vinto){
            	System.out.println("Prima lettura");
                int vincita = (Integer)input.readObject();
            	System.out.println("Dopo lettura");

                clientGUI.updateLogArea("hai vinto " + vincita);
                clientGUI.updateConnectionInfo("hai vinto " +vincita);
				clientGUI.setCredito(clientGUI.getCredito()+vincita);
				clientGUI.updateConnectionInfo("Il tuo credito è ora di:  " + clientGUI.getCredito() + " pandaCoin");
				clientGUI.updateLogArea("Il tuo credito è ora di:  " + clientGUI.getCredito() + " pandaCoin");
            } else {
            	String balance=(String) input.readObject();
            	clientGUI.updateLogArea("hai perso " + clientGUI.getPuntata());
                clientGUI.updateConnectionInfo("hai perso " + clientGUI.getPuntata());
                clientGUI.setCredito(clientGUI.getCredito()-clientGUI.getPuntata());
                clientGUI.updateConnectionInfo("Il tuo credito è ora di:  " + clientGUI.getCredito() + " pandaCoin");
				clientGUI.updateLogArea("Il tuo credito è ora di:  " + clientGUI.getCredito() + " pandaCoin");
            }
            closeConnection();
            if(clientGUI.getCredito()==0) {
            	clientGUI.updateLogArea("Utente eliminato!");
            	closeConnection();
            } else {
            	int scelta = JOptionPane.showConfirmDialog(null,"Nuova partita?");
                if (scelta ==0){
    				System.out.println("scelta =" + scelta);
    				clientGUI.resetGUI();
                    runClient(3);
                }
            }
            	
		}

		// System.out.println("MESSAGGIO: "+message);

	}

	void closeConnection() {
		try {
			output.close();
			input.close();
			client.close();

		} catch (IOException io) {

		}

	}

	public static void main(String asdsa[]) {

		try {
			// Set cross-platform Java L&F (also called "Metal")
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		}
		catch (UnsupportedLookAndFeelException e) {
			// handle exception
		}
		catch (ClassNotFoundException e) {
			// handle exception
		}
		catch (InstantiationException e) {
			// handle exception
		}
		catch (IllegalAccessException e) {
			// handle exception
		}

        Client client = new Client("localhost");  //Create and show the GUI.
        client.runClient(0);

	}




}
