package pandavallo;

public class LogicaGioco {

	Cavallo c1, c2, c3, c4;
	public static final int ROSSO=1;
	public static final int NERO=2;
	public static final int BLU=3;
	public static final int VERDE=4;
	//CAVALLI ROSSO1, NERO2, BLU3, VERDE4
	// "1,1" cavallo 1 un passo avanti
	// "1,0" cavallo 1 un passo indietro

	int colonna;
	int[] carteSopra=new int[7];



	public LogicaGioco() {
		c1=new Cavallo(ROSSO);
		c2=new Cavallo(NERO);
		c3=new Cavallo(BLU);
		c4=new Cavallo(VERDE);

		colonna=1;
		for(int i=0;i<7;i++)
			carteSopra[i]=(int) (Math.random()*4)+1;
	}


	public String mossa() {

		int estrazioneRandom=(int) (Math.random()*4)+1;

		switch(estrazioneRandom) {
		case ROSSO: c1.passoAvanti(); break;
		case NERO: 	c2.passoAvanti(); break;
		case BLU: 	c3.passoAvanti(); break;
		case VERDE: c4.passoAvanti(); break;
		}

		return estrazioneRandom+",1,"+colonna;

	}


	public String  controlloColonna() {
		if(c1.getPosizione()>colonna && c2.getPosizione()>colonna && c3.getPosizione()>colonna && c4.getPosizione()>colonna) {

			int chi=0;

			switch(carteSopra[colonna-1]) {
			case ROSSO: chi=ROSSO; 	c1.passoIndietro(); break;
			case NERO: 	chi=NERO; 	c2.passoIndietro(); break;
			case BLU: 	chi=BLU; 	c3.passoIndietro(); break;
			case VERDE: chi=VERDE; 	c4.passoIndietro(); break;
			}
			colonna++;

			return chi+",0,"+colonna;
		} 
		else return null;
	}

	public String qualcunoVince() {
		int chi=0;
		if(c1.haVinto()) chi=ROSSO;
		else if(c2.haVinto()) chi=NERO;
		else if(c3.haVinto()) chi=BLU;
		else if(c4.haVinto()) chi=VERDE;

		if(chi==0)
			return null;
		else
			return chi+",2,"+colonna;
	}

}