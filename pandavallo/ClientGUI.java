package pandavallo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

/**
 * Created by patrick on 20/01/2016.
 */
public class ClientGUI extends ClientLogica {
    JFrame jFrame = new JFrame();
    JPanel jPanelGioco = new JPanel();
    JPanel jPanelInfo = new JPanel();
   /* JpanelGioco*/

    JLabel rosso = new JLabel();
    JLabel nero = new JLabel();
    JLabel blu = new JLabel();

    JLabel verde = new JLabel();
    JLabel carta1 = new JLabel();
    JLabel carta2 = new JLabel();
    JLabel carta3 = new JLabel();
    JLabel carta4 = new JLabel();
    JLabel carta5 = new JLabel();
    JLabel carta6 = new JLabel();
    JLabel carta7 = new JLabel();

    JLabel carte[];
    JTextArea connectionInfo = new JTextArea();
    JScrollPane scroll = new JScrollPane(connectionInfo);

    Icon cavalloRosso = new ImageIcon(this.getClass().getResource("/images/rosso.jpg"));
    Icon cavalloNero = new ImageIcon(this.getClass().getResource("/images/nero.jpg"));
    Icon cavalloBlu = new ImageIcon(this.getClass().getResource("/images/blu.jpg"));
    Icon cavalloVerde = new ImageIcon(this.getClass().getResource("/images/verde.jpg"));
    Icon scopri = new ImageIcon(this.getClass().getResource("/images/scopri.jpg"));
    Icon penalitaRosso = new ImageIcon(this.getClass().getResource("/images/penalitaRosso.png"));
    Icon penalitaNero = new ImageIcon(this.getClass().getResource("/images/penalitaNero.png"));
    Icon penalitaBlu = new ImageIcon(this.getClass().getResource("/images/penalitaBlu.png"));
    Icon penalitaVerde = new ImageIcon(this.getClass().getResource("/images/penalitaVerde.png"));

    JPanel grid[] = new JPanel[45]; /* JPanel  info*/
    JTextField puntata = new JTextField();
    JTextField username = new JTextField();
    JTextField infoCavallo = new JTextField(); /* JTextField Soldi = new JTextField ();*/

    ClientGUI() {


    }

    JTextArea logTxtArea = new JTextArea();

    public void logGUI(){
        JFrame logJFrame = new JFrame();
        JScrollPane scroll = new JScrollPane(logTxtArea);

        scroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());

            }
        });
        logJFrame.getContentPane().add(scroll, BorderLayout.CENTER);
        logTxtArea.setEditable(false);

        logTxtArea.setText("Benvenuto");


        logJFrame.getContentPane().setLayout(new BorderLayout());
        logJFrame.getContentPane().add(logTxtArea,BorderLayout.CENTER);

        logJFrame.setSize(500,500);
        logJFrame.setVisible(true);
        logJFrame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);





    }

    public void gameGUI() {

        /*impostiamo il jFrame*/

        jFrame.setLayout(new BorderLayout());
        jFrame.add(jPanelGioco, BorderLayout.NORTH);
        jFrame.add(jPanelInfo, BorderLayout.CENTER);
        jFrame.setVisible(true);
        jFrame.setSize(900, 700);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame.setLocation(dim.width / 2 - jFrame.getSize().width / 2, dim.height / 2 - jFrame.getSize().height / 2); /* queste due righe sopra sono per aprorlo al centro dello schermo*/
        jFrame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); /*impostiamo il jPanelGioco*/
        jPanelGioco.setLayout(new GridLayout(5, 8));
        jPanelGioco.setVisible(true);
        for (int i = 0; i < 45; i++) {
            String a = "" + i;
            JPanel j = new JPanel();
            j.setName(a);
            j.setBackground(Color.WHITE);
            grid[i] = j;
            jPanelGioco.add(j);
        } /*impostiamo i cavalli*/
        JPanel PanelRosso = grid[getGridPosition(ROSSO)];
        JPanel PanelNero = grid[getGridPosition(NERO)];
        JPanel PanelBlu = grid[getGridPosition(BLU)];
        JPanel PanelVerde = grid[getGridPosition(VERDE)];
        rosso.setIcon(cavalloRosso);
        nero.setIcon(cavalloNero);
        blu.setIcon(cavalloBlu);
        verde.setIcon(cavalloVerde);
        PanelRosso.add(rosso);
        PanelNero.add(nero);
        PanelBlu.add(blu);
        PanelVerde.add(verde); /*impostiamo le carte*/
        carta1.setIcon(scopri);
        carta2.setIcon(scopri);
        carta3.setIcon(scopri);
        carta4.setIcon(scopri);
        carta5.setIcon(scopri);
        carta6.setIcon(scopri);
        carta7.setIcon(scopri);
        JPanel c1 = grid[1];
        JPanel c2 = grid[2];
        JPanel c3 = grid[3];
        JPanel c4 = grid[4];
        JPanel c5 = grid[5];
        JPanel c6 = grid[6];
        JPanel c7 = grid[7];
        c1.add(carta1);
        c2.add(carta2);
        c3.add(carta3);
        c4.add(carta4);
        c5.add(carta5);
        c6.add(carta6);
        c7.add(carta7);
        /* le metto in un array*/
        carte = new JLabel[]{carta1, carta2, carta3, carta4, carta5, carta6, carta7};
        jPanelGioco.updateUI(); /*_______________________ JpanelInfo*/
        jPanelInfo.setLayout(new BorderLayout());
        username.setEditable(false);
        username.setBorder(BorderFactory.createEmptyBorder());
        puntata.setEditable(false);
        puntata.setBorder(BorderFactory.createEmptyBorder());
        infoCavallo.setEditable(false);
        infoCavallo.setBorder(BorderFactory.createEmptyBorder());

        username.setText(getUtente());
        JPanel info = new JPanel(new FlowLayout());
        info.add(username);
        info.add(infoCavallo);
        info.add(puntata);

        jPanelInfo.add(info, BorderLayout.NORTH);
        /*impostiamo la text area*/
        connectionInfo.setEditable(false);
        connectionInfo.setText("BENVENUTO!!");
        scroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());

            }
        });


        jPanelInfo.add(scroll, BorderLayout.CENTER);


    }


    public int  loginGUI(){
          /*
    * faccimao che torna 1 se si logga
    * 2 se crea un nuovo utente
    *
    * 0 se chiude
    */

        int r;
        r=new SceltaDialog().getScelta();
        System.out.println("scelta: "+r);

        if (r==1) {

            r = new LoginDialog(this).getAzione();
        }else if (r==2) new NewUserDialog(this).getAzione();
        return r;

    }

    public int puntaGUI() {
        int p = new PuntataDialog(this).getPuntata();
        return p;


    }


    public void updateCavalli() {



        JPanel PanelRosso = grid[getGridPosition(ROSSO)];
        JPanel PanelNero = grid[getGridPosition(NERO)];
        JPanel PanelBlu = grid[getGridPosition(BLU)];
        JPanel PanelVerde = grid[getGridPosition(VERDE)];
        PanelRosso.add(rosso);
        PanelNero.add(nero);
        PanelBlu.add(blu);
        PanelVerde.add(verde);

        jPanelGioco.repaint();

        jPanelGioco.validate();

    }

    public void updateCarte (  int mossa, int cavallo, int colonna){
        if (mossa == 0) {
            System.out.println("entrato");
            switch (cavallo) {
                case ROSSO:
                    carte[colonna - 2].setIcon(penalitaRosso);
                    break;
                case NERO:
                    carte[colonna - 2].setIcon(penalitaNero);
                    break;
                case BLU:
                    carte[colonna - 2].setIcon(penalitaBlu);
                    break;
                case VERDE:
                    carte[colonna - 2].setIcon(penalitaVerde);
                    break;

                default:

            }
        }
        jPanelGioco.repaint();

        jPanelGioco.validate();
    }







    public void updateConnectionInfo(String s) {

        connectionInfo.append("\n" + s);

    }

    public void updateLogArea(String s){
        logTxtArea.append("\n"+s);
    }

    public void resetGUI(){
        reset();
        jFrame.invalidate();
        jFrame.getContentPane().removeAll();

        jPanelGioco.removeAll();

        jPanelInfo.removeAll();
        gameGUI();
        jFrame.repaint();
        jFrame.validate();



    }





}
