package pandavallo;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.function.BooleanSupplier;

/**
 * Created by patrick on 16/02/16.
 */
public class ServerLogin extends Thread {
	private Socket client;
	ObjectOutputStream outputs;
	ObjectInputStream inputs;
	ClientSocket clientSocket = new ClientSocket();
	String credito;
    ArrayList<Utente> utenteArrayList;
    Utente user = new Utente();
    Boolean userAlreadyLogged = false;



	boolean logged = false;



	public ServerLogin(Socket client, ArrayList<Utente> utenteArrayList) {
		super();
        this.utenteArrayList = utenteArrayList;
		this.client = client;
		start();
	}

	@Override
	public void run() {
		try {

			outputs = new ObjectOutputStream(client.getOutputStream());
			inputs = new ObjectInputStream(client.getInputStream());
			clientSocket.setObjectInputStream(inputs);
			clientSocket.setObjectOutputStream(outputs);

			String log = (String) inputs.readObject();

            String[] login = log.split(",");
            int type = Integer.parseInt(login[0]);
            String username = login[1];

        int errore=0;//1 = user o pw sbagliati, 2 = utente già esistente, 3= utente già loggato

            if (type == 1){
				System.out.println("entrato");

                for(int i=0; i<utenteArrayList.size();i++){
					System.out.println("nome utente: "+utenteArrayList.get(i).getNome());

                    if(username.equals(utenteArrayList.get(i).getNome())){
                        userAlreadyLogged = true;
                        errore = 3;

                    }

                }

            }


			if (processLogin(log) && !userAlreadyLogged) {

				outputs.writeObject("LOGGED,"+credito);

				setLogged(true);
				user.setNome(username);

                if (type ==1) errore = 1;
                else errore  =2;

                /*
                * se entra qui con type  == 1 significa che l'utente non sta già giocando
                * quindi l'unico errore possibile è che sbagli user o pw
                *
                * se entra con 2 può solo mettere lo user di un utente già esistente
                * */


			} else {
                switch (errore){
                    case 1:outputs.writeObject("NOTLOGGED");
                        break;
                    case 2: outputs.writeObject("USER ALREADY EXIT");
                        break;
                    case 3: outputs.writeObject("USER ALREADY LOGGED");
                        break;
                }


				setLogged(false);
				inputs.close();
				outputs.close();
				client = null;
			}
			this.interrupt();



		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}



	}

	private boolean processLogin(String log) {
		String[] login = log.split(",");
		int type = Integer.parseInt(login[0]);
		String username = login[1];
		String password = login[2];


		switch (type) {
		//LOGIN
		case 1:
			return readFromFile(username, password);


			//NUOVO UTENTE
		case 2:
			return writeToFile(username, password);
		default:
			return false;
		}

	}

	private boolean readFromFile(String username, String password) {

		String f = String.valueOf(this.getClass().getResource("/db/database.pandav"));
		File file;
		
		if(System.getProperty("os.name").equals("Linux"))
			file = new File(f.split(":")[1]);
		else
			file = new File(f.split("file:/")[1]);
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);


			boolean logged = false;

			while (!logged) {
				String user = br.readLine();
				//user.replaceAll("\\s","");
				if (user == null)   break;
				String[] uinfo = user.split("~");
				if (uinfo[0].equals(username) && uinfo[1].equals(password)){
					logged = true;
					credito = uinfo[2];

				}

			}


			if (logged) {
				br.close();
				return true;
			} else {
				br.close();
				return false;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return false;

		}


	}
	private boolean writeToFile(String username, String password) {
		String f = String.valueOf(this.getClass().getResource("/db/database.pandav"));

		File file;

		if(System.getProperty("os.name").equals("Linux"))
			file = new File(f.split(":")[1]);
		else
			file = new File(f.split("file:/")[1]);
		
		try {
			FileWriter fw=new FileWriter(file, true);
			BufferedWriter bw=new BufferedWriter(fw);

			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			boolean sameUser = false;

			while (! sameUser) {
				String user = br.readLine();
				//user.replaceAll("\\s","");
				if (user == null) break;
				String[] uinfo = user.split("~");
				if (uinfo[0].equals(username) ) {
					sameUser = true;

				}
			}
			//ritorna false se esiste già un utente con lo stesso nome
			if (sameUser){
				return false;


			}else {

				synchronized (bw) {
					bw.append(username + "~" + password + "~20" + "\n");
					bw.close();
					credito = "20";
					return true;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
        return false;

	}
	public boolean isLogged() {
		return logged;
	}
    public Utente getUtente(){
        return user;
    }

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public Socket returnClient(){return client;}

	public ClientSocket getClientSocket(){
		return clientSocket;
	}
}
