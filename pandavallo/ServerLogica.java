package pandavallo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerLogica extends Thread {

	private LogicaGioco logica;

	ObjectOutputStream[] outputs;
	ObjectInputStream[] inputs;
    int numeroClients;
	private Utente user[];
	private int numeroVincitori = 0;




	public ServerLogica(ClientSocket clientSocket, int numeroClients) {
		super();

        this.numeroClients = numeroClients;
		outputs=clientSocket.getObjectOutputStreamsLogged();
		inputs=clientSocket.getObjectInputStreamsLogged();


        System.out.println(numeroClients);
        logica=new LogicaGioco();
		start();

	}

	@Override
	public void run() {
		try {


            user=new Utente[numeroClients];

			for(int i=0; i<numeroClients;i++) {




					outputs[i].writeObject("START");
                outputs[i].flush();

                String info = (String) inputs[i].readObject();
                System.out.println(info);
                String[] subinf = info.split(",");
                //USER, CAVALLO, PUNTATA
                user[i] = new Utente(subinf[0], Integer.parseInt(subinf[2]),Integer.parseInt(subinf[1]));
                outputs[i].writeObject(numeroClients);
                outputs[i].flush();

            }

            for (int k = 0; k< numeroClients;k++){
                for(int i=0; i<numeroClients;i++){
                    String info = user[i].getNome()+","+user[i].getCavallo()+","+user[i].getPuntata();

                    outputs[k].writeObject(info);
                    outputs[k].flush();
                }

            }




			while(logica.qualcunoVince()==null) {
				String mossa=logica.mossa();
				for(int i=0;i<numeroClients;i++) {
                        outputs[i].writeObject(mossa);
                        outputs[i].flush();
                    System.out.println("mossse inviate: "+ mossa);
                }
				Thread.sleep(500);

				String haVinto=logica.qualcunoVince();
				if(haVinto!=null) {
					for(int i=0;i<numeroClients;i++) {
                            outputs[i].writeObject(haVinto);
                            outputs[i].flush();

					}
					break;
				}

				Thread.sleep(500);

				String colonna=logica.controlloColonna();
				if(colonna!=null) {
					for(int i=0;i<numeroClients;i++) {
                        outputs[i].writeObject(colonna);
                        outputs[i].flush();

					}
				}

				Thread.sleep(500);
			}


            utenteVincente(Integer.parseInt(logica.qualcunoVince().split(",")[0]));
			this.interrupt();


		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }



	
	//FACSIMILE del metodo da usare per aggiornare i soldi di un utente senza usare RandomAccessFile
	//RAFile era comodo in BankUI perche' c'era l'accountnumber che facilitava il seek(), qui
	//non abbiamo un counter degli utenti e quindi RAFile diventa scomodo da usare
	//TODO DA FINIRE
	private void updateMoney(String username, int variance) {
		File database;
		File tmp;
		
		if(System.getProperty("os.name").equals("Linux")) {
			database=new File(String.valueOf(this.getClass().getResource("/db/database.pandav")).split(":")[1]);
			tmp=new File(String.valueOf(this.getClass().getResource("/db/tmp.pandav")).split(":")[1]);
		} else {
			database=new File(String.valueOf(this.getClass().getResource("/db/database.pandav")).split("file:/")[1]);
			tmp=new File(String.valueOf(this.getClass().getResource("/db/tmp.pandav")).split("file:/")[1]);
		}
		FileReader fr;
		try {
			fr = new FileReader(database);
			BufferedReader br=new BufferedReader(fr);
			
			FileWriter fw=new FileWriter(tmp);
			BufferedWriter bw=new BufferedWriter(fw);
			
			while(true) {
				String line=br.readLine();
				if(line==null) break;
				String[] info=line.split("~");
				if(info[0].equals(username)) {
					int actualMoney=Integer.parseInt(info[2]);
					if((actualMoney+variance)<=0) continue;
					line=info[0]+"~"+info[1]+"~"+(actualMoney+variance);
					
				}
				synchronized (tmp) {
					bw.write(line+"\n");
				}
				
					
			}
			
			br.close();
			bw.close();
			
			fr = new FileReader(tmp);
			br=new BufferedReader(fr);
			
			fw=new FileWriter(database);
			bw=new BufferedWriter(fw);
			
			while(true) {
				String line=br.readLine();
				if(line==null) break;
				synchronized (database) {
					bw.write(line+"\n");
				}
				
					
			}
			
			br.close();
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}








    public void utenteVincente(int cavalloVincente) {

//                    utenteVincente(Integer.parseInt(haVinto.split(",")[0]));


		for (int i = 0; i < user.length; i++) {
			System.out.println(user[i].getCavallo()+"=="+cavalloVincente);
            if (user[i].getCavallo() == cavalloVincente) {
            	
                user[i].setVinto(true);
                numeroVincitori++;
            }
        }

        for (int i = 0; i < user.length; i++) {
        	System.out.println(user[i].getNome()+" "+user[i].isVinto());
            if (user[i].isVinto()){
                int vincita =user[i].getPuntata() * user.length / numeroVincitori;

                user[i].setCredito(user[i].getCredito()+vincita);
                System.out.println("Prima scrittura file");
                updateMoney(user[i].getNome(),vincita);
                System.out.println("Dopo scrittura file");
                try {
                	System.out.println("Prima scrittura");
                    outputs[i].writeObject(vincita);
                	System.out.println("Dopo scrittura");

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
            	user[i].setCredito(user[i].getCredito()-user[i].getPuntata());
            	updateMoney(user[i].getNome(),-user[i].getPuntata());
            	
            	try {
                	System.out.println("Prima scrittura");
                    outputs[i].writeObject(""+user[i].getCredito());
                	System.out.println("Dopo scrittura");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

			/*

			   i vincitori prendono il (valore della puntata * il numero degli utenti)/numero vincitori
			   questo è sommato al loro credito

			   //mandare all'utente quando ha vinto


			 */

    }

	public Utente[] getUser(){return user;}
}
