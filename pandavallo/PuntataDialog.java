package pandavallo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by patrick on 02/02/16.
 */
public class PuntataDialog extends JDialog implements ActionListener {
    ClientGUI clientGUI = new ClientGUI();

    JPanel jPanelSelezionaCavallo = new JPanel();
    JPanel jPanelInserisciPuntata = new JPanel();
    JTextField cavalloSelezionato = new JTextField();
    JTextField puntataField = new JTextField();
    JTextField infopuntata = new JTextField();
    JButton puntaButton = new JButton();

    public PuntataDialog(ClientGUI clientGUI) {

        setModal(true);

        puntaButton.setEnabled(false);


        getContentPane().setLayout(new BorderLayout());
        setSize(400, 250);


        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);

        JPanel center = new JPanel();
        cavalloSelezionato.setText("Seleziona il cavallo");
        cavalloSelezionato.setColumns(32);
        cavalloSelezionato.setEditable(false);
        cavalloSelezionato.setHorizontalAlignment(SwingConstants.CENTER);
        cavalloSelezionato.setBorder(BorderFactory.createEmptyBorder());
        center.add(cavalloSelezionato);

        getContentPane().add(center, BorderLayout.CENTER);
        getContentPane().add(jPanelSelezionaCavallo, BorderLayout.NORTH);
        getContentPane().add(jPanelInserisciPuntata, BorderLayout.SOUTH);


        // selezionare il cavallo

        jPanelSelezionaCavallo.setLayout(new GridLayout(1, 4));

        JPanel grid[] = new JPanel[4];
        for (int i = 0; i < 4; i++) {
            JPanel j = new JPanel();

            j.setBackground(Color.WHITE);

            grid[i] = j;
            jPanelSelezionaCavallo.add(j);
        }
        JPanel jPanelRosso = grid[0];
        JPanel jPanelNero = grid[1];
        JPanel jPanelBlu = grid[2];
        JPanel jPanelVerde = grid[3];

        // copio le label create precedentemente per poter aggiungere l'action listener
        JButton cRosso = new JButton();
        JButton cNero = new JButton();
        JButton cBlu = new JButton();
        JButton cVerde = new JButton();

        cRosso.setIcon(clientGUI.cavalloRosso);
        cRosso.setBorder(BorderFactory.createEmptyBorder());

        cNero.setIcon(clientGUI.cavalloNero);
        cNero.setBorder(BorderFactory.createEmptyBorder());

        cBlu.setIcon(clientGUI.cavalloBlu);
        cBlu.setBorder(BorderFactory.createEmptyBorder());

        cVerde.setIcon(clientGUI.cavalloVerde);
        cVerde.setBorder(BorderFactory.createEmptyBorder());


        jPanelRosso.add(cRosso);
        jPanelNero.add(cNero);
        jPanelBlu.add(cBlu);
        jPanelVerde.add(cVerde);

        cRosso.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientGUI.setCabballo(clientGUI.ROSSO);

                cavalloSelezionato.setText("hai selezionato il cavallo ROSSO");

                puntaButton.setEnabled(true);
            }
        });
        cNero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                clientGUI.setCabballo(clientGUI.NERO);
                cavalloSelezionato.setText("hai selezionato il cavallo NERO");
                puntaButton.setEnabled(true);

            }
        });
        cBlu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                clientGUI.setCabballo(clientGUI.BLU);
                cavalloSelezionato.setText("hai selezionato il cavallo BLU");
                puntaButton.setEnabled(true);
            }
        });
        cVerde.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                clientGUI.setCabballo(clientGUI.VERDE);
                cavalloSelezionato.setText("hai seleziohato il cavallo VERDE");
                puntaButton.setEnabled(true);
            }
        });


        // impostiamo la puntata

        infopuntata.setText("inserisci la puntata");
        infopuntata.setEditable(false);
        infopuntata.setHorizontalAlignment(SwingConstants.CENTER);
        infopuntata.setBorder(BorderFactory.createEmptyBorder());

        puntataField.setText("10");

        puntaButton.setText("PUNTA");

        puntaButton.addActionListener(this);

        jPanelInserisciPuntata.add(infopuntata);
        jPanelInserisciPuntata.add(puntataField);
        jPanelInserisciPuntata.add(puntaButton);
        clientGUI.jPanelInfo.repaint();
        setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        int p = Integer.parseInt(puntataField.getText());

        clientGUI.setPuntata(p);
        dispose();


        clientGUI.puntata.setText("puntando: " + puntataField.getText());
        clientGUI.infoCavallo.setText(cavalloSelezionato.getText());
        clientGUI.jPanelInfo.repaint();
        clientGUI.jPanelInfo.validate();
    }

    public int getPuntata() {
        return clientGUI.getPuntata();
    }
}
