package pandavallo;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Created by patrick on 17/02/16.
 */
public class ClientSocket {

    private ObjectOutputStream[] objectOutputStreamsLogged =  new ObjectOutputStream[10];
    private ObjectInputStream[] objectInputStreamsLogged= new ObjectInputStream[10];
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;

    public ObjectOutputStream[] getObjectOutputStreamsLogged() {
        return objectOutputStreamsLogged;
    }

    public void setObjectOutputStreamsLogged(ObjectOutputStream objectOutputStreamsLogged, int i ) {
        this.objectOutputStreamsLogged[i] = objectOutputStreamsLogged;
    }

    public ObjectInputStream[] getObjectInputStreamsLogged() {
        return objectInputStreamsLogged;
    }

    public void setObjectInputStreamsLogged(ObjectInputStream objectInputStreamsLogged, int i) {
        this.objectInputStreamsLogged[i] = objectInputStreamsLogged;
    }

    public ObjectOutputStream getObjectOutputStream() {
        return objectOutputStream;
    }

    public void setObjectOutputStream(ObjectOutputStream objectOutputStream) {
        this.objectOutputStream = objectOutputStream;
    }

    public ObjectInputStream getObjectInputStream() {
        return objectInputStream;
    }

    public void setObjectInputStream(ObjectInputStream objectInputStream) {
        this.objectInputStream = objectInputStream;
    }





}
